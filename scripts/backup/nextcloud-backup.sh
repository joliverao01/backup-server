#!/bin/bash
user=$(who | cut -d ' ' -f 1)
echo "
 ---------------------------------------------------------------
 -                  NEXTCLOUD BACKUP                           -
 ---------------------------------------------------------------
"
echo " EXECUTING THE FULL BACK UP FOR THE NEXTCLOUD MACHINE"
sleep 1
echo " ."
sleep 1
echo " .."
sleep 1
echo " ..."
sleep 1
echo " Here we go"
cd /
sleep 1
sudo tar -cvpzf backup.tar.gz --exclude=/backup.tar.gz --one-file-system / | nc -q 0 10.35.0.167 1024
sudo chmod 600 /etc/crontab
sudo echo "30  5    * * sun root    /home/$user/nextcloud-backup.sh" >> /etc/crontab
                        
echo "BACKUP DONE"
echo "EXITING"
exit
