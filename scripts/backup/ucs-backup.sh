#!/bin/bash
user=$(who | cut -d ' ' -f 1)

echo "
 ---------------------------------------------------------------
 -                  UCS BACKUP                           -
 ---------------------------------------------------------------
"
echo " EXECUTING THE FULL BACK UP FOR THE UCS MACHINE"
sleep 1
echo " ."
sleep 1
echo " .."
sleep 1
echo " ..."
sleep 1
echo " HERE WE GO"
cd /
sleep 1
sudo tar -cvpzf backup.tar.gz --exclude=/backup.tar.gz --one-file-system / | nc -q 0 10.1.1.20 1024
sudo chmod 600 /etc/crontab
sudo echo "30  6    * * sun root    /home/$user/ucs-backup.sh" >> /etc/crontab

echo "BACKUP DONE"
echo "EXITING"
exit
