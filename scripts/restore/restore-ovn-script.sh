#!/bin/bash
clear
while [ option!=0 ]
do
echo ""
echo " ----------------------------------------------------------------------"
echo "             RESTORING A BACKUP OVER THE NETWORK                     "
echo " ----------------------------------------------------------------------"
echo "   1. Receive Restore #ONLY IN THE MACHINE THAT YOU WANT TO RESTORE! "
echo "   2. Select the machine for restore                                 "
echo "   0. Return                                                         "
echo " ----------------------------------------------------------------------"
echo ""
echo " For start:"
echo " ON THE MACHINE THAT YOU WANT TO RESTORE EXECUTE THE FIRST OPTION"
echo " SELECT THE MACHINE THAT YOU WANT TO RESTORE"
echo ""
read -p " Choose an option: " option;

case $option in
  0)
    clear
    break
  ;;
  1)
    echo " Waiting for an incoming backup..."
    nc -l 1024 | sudo tar -xvpzf - -C /media/whatever
  ;;
  2)
  while [ option1!=0 ]
  do
  echo ""
  echo " ----------------------------------------------------------------------"
  echo "                       SELECT THE MACHINE                            "
  echo " ----------------------------------------------------------------------"
  echo "   1. Nextcloud                                                      "
  echo "   2. Mail                                                           "
  echo "   3. UCS                                                            "
  echo "   0. Return                                                         "
  echo " ----------------------------------------------------------------------"
  echo ""
  read -p " Choose an option: " option1;

  case $option1 in
    0)
      clear
      break
    ;;
    1)
      read -p " Tell me the ip of nextcloud machine: " ip;
      cat /home/koffietest/nextcloud/backup.tar.gz | nc -q 0 $ip 1024
    ;;
    2)
      read -p " Tell me the ip of mail machine: " ip;
      cat /home/koffietest/mail/backup.tar.gz | nc -q 0 $ip 1024
    ;;
    3)
      read -p " Tell me the ip of ucs machine: " ip;
      cat /home/koffietest/ucs/backup.tar.gz | nc -q 0 $ip 1024
    ;;
  esac
  done
  ;;
esac
done


exit
