#!/bin/bash
echo ""
echo " ---------------------------------------------------------------"
echo "           RECEIVING MAIL BACKUP                         "
echo " ---------------------------------------------------------------"
echo ""
cd
echo " WAITING FOR RECEIVE THE BACKUP BY THE MAIL MACHINE"
nc -l 1024 > backup.tar.gz
echo " I'VE RECEIVED IT... STORING IN /home/koffietest/backups/mail-backup/"
echo " I'VE FOUND ANOTHER ARCHIVE WITH THE SAME NAME, I'LL DELETE IT..."
cd /home/koffietest/backups/mail-backup/
if [ -e "backup.tar.gz" ]
then
  echo " I'VE FOUND AN ARCHIVE WITH THE SAME NAME... DELETING IT..."
  rm backup.tar.gz
  cd
  echo ""
  echo " TRANSFERING THE BACKUP TO /home/koffietest/backups/mail-backup/..."
  mv backup.tar.gz /home/koffietest/backups/mail-backup/
  ll
  echo " THE WORK IS DONE, EXITING..."
  sleep 2
  exit
else
  echo " I'VE NOT FOUND ANOTHER ARCHIVE IN THIS FOLDER"
  echo " TRANSFERING THE BACKUP TO /home/koffietest/backups/mail-backup/..."
  cd
  mv backup.tar.gz /home/koffietest/backups/mail-backup/
  ll
  echo " THE WORK IS DONE, EXITING..."
  sleep 2
  exit
fi
