#!/bin/bash
user=$(who | cut -d ' ' -f 1)
echo "
 ---------------------------------------------------------------
                   RECEIVE BACKUP                             
 ---------------------------------------------------------------
"
echo " ---------------------------------------------------------------"
echo " Creating the folders in /home/koffietest..."
echo " ---------------------------------------------------------------"
ls
mkdir /home/$user/backups
ls
echo " ---------------------------------------------------------------"
echo " As you can see, I've created the folder 'backups'."
echo " ---------------------------------------------------------------"
sleep 3
echo " ---------------------------------------------------------------"
echo " Going to create the rest of the folders..."
echo " ---------------------------------------------------------------"
cd /home/$user/backups
mkdir nextcloud-backup
mkdir mail-backup
mkdir ucs-backup
ls
echo " ---------------------------------------------------------------"
echo " Now, we have three folders for store the backups."
echo " ---------------------------------------------------------------"
sleep 2
echo " Changing the contrab to execute the order of receive the backups on Sunday..."
echo " ---------------------------------------------------------------"
sudo chmod 607 /etc/crontab
sudo echo "30  5    * * sun root    /home/$user/nextcloud-receive.sh" >> /etc/crontab
sudo echo "*   6    * * sun root    /home/$user/mail-receive.sh" >> /etc/crontab
sudo echo "30  6    * * sun root    /home/$user/ucs-receive.sh" >> /etc/crontab
sudo cat /etc/crontab | grep .sh
echo ""
echo "DONE, GOING BACK..."
sleep 7
clear
exit
