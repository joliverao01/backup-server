 ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ 
||I |||N |||S |||T |||R |||U |||C |||T |||I |||O |||N |||S ||
||__|||__|||__|||__|||__|||__|||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|
 ____ ____ ____ _________ ____ ____ ____ _________ ____ ____ ____ 
||F |||O |||R |||       |||U |||S |||E |||       |||T |||H |||E ||
||__|||__|||__|||_______|||__|||__|||__|||_______|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/_______\|/__\|/__\|/__\|
 ____ ____ ____ ____ ____ ____ 
||S |||C |||R |||I |||P |||T ||
||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|

PLEASE, IF YOU DON'T KNOW WHAT ARE YOU DOING, DON'T DO IT

1. Opening
 - To open the script, you have to open a terminal (CTRL+ALT+T) and go to the path where "backup-script.sh" is located.
 - To list the documents you can run the commands "ll" or "ls".
 - To change the working directory, you can use the command "cd 'path'" (On path, you have to put the directory where you want to go).
 - For launching the script, run "./backup-script.sh" (only if you are in the directory where the script is located).
 - If you don't have permissions to launch it, you can run "chmod u+x backup-script.sh" it will change the permissions of the file and will   	allow you to execute it.

2. Choosing
 - For choose an option, you only have to choose the number of it and follow the steps.

3. Archives
 - We recommend u to don't move or edit any script, because the main script (backup-script.sh) is linked with the actual paths. If you edit or 	move something, ensure that it will works.
 - EDIT THE FILES AT YOUR OWN RISK

The script was made by the Spanish guys, Joel Olivera, Alejandro Mallén and Oriol.
If you have any problem with it, you can ask to this mail: "joliverao01@elpuig.xeill.net" or "amalleng01@elpuig.xeill.net"

