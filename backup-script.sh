#!/bin/bash
clear;

user=$(who | cut -d ' ' -f 1)
option=0;

while [ option!=0 ]
do
	echo " ";
	echo    " _____________________________ ";
	echo	"|                             |";
	echo -e "| \e[36m-------\e[0m \e[5mBACK UP MENU\e[0m \e[36m------\e[0m |";
	echo -e "|_____________________________|";
	echo    "|                             |";
	echo -e "|\e[0;34m 1) Full Backup\e[0m              |";
	echo -e "| 2) Restore Backup           |";
	echo -e "|\e[0;31m 0) Exit      \e[0m               |";
	echo    "|_____________________________|";
	echo " ";

	read -p "Choose an option: " option;

	case $option in
	0)
		clear;
		echo "Bye..."
		exit 0;
	;;
	1)
		while [ $option1!=0 ]
		do
		clear;
		echo " ";
		echo "
 --------------------------------------------------------
                       Full Backup
 --------------------------------------------------------
  1) Receive Backup #ONLY IN THE TEST MACHINE!
  2) Backup Nextcloud
  3) Backup Mail
  4) Backup UCS
	5) Manually Receive
	6) Manually Backup
  0) Return
 --------------------------------------------------------
	 "
		echo "I need the permissions to execute the scripts."
		echo "You have to be root."
		sudo chmod u+rwx scripts
		echo ""
		read -p "Choose one option: " option1;
		case $option1 in

			0)
				clear;
				break;
			;;

			1)
				clear;
				sudo chmod u+x scripts/receive/receive-backup.sh
				./scripts/receive/receive-backup.sh
				break
			;;
			2)
				clear;
				sudo chmod u+x scripts/backup/nextcloud-backup.sh
				./scripts/backup/nextcloud-backup.sh
				break
			;;
			3)
				clear;
				sudo chmod u+x scripts/backup/mail-backup.sh
				./scripts/backup/mail-backup.sh
				break
			;;
			4)
				clear;
				sudo chmod u+x scripts/backup/ucs-backup.sh
				./scripts/backup/ucs-backup.sh
				break
			;;
			5)
			echo "
			 ---------------------------------------------------------------
			 -                  MANUALLY RECEIVE                           -
			 ---------------------------------------------------------------
			"
			echo " WAITING FOR RECEIVE THE BACKUP MANUALLY"
			nc -l 1024 > backup.tar.gz
			;;
			6)
			echo "
			 ---------------------------------------------------------------
			 -                  MANUALLY BACKUP                           -
			 ---------------------------------------------------------------
			"
			read -p "Tell me the IP that will receive the backup" ip;
			sudo tar -cvpzf backup.tar.gz --exclude=/backup.tar.gz --one-file-system / | nc -q 0 $ip 1024
			;;
		esac
		done
	;;
	2)
		while [ $option2!=0 ]
		do
		clear;
		echo "
 -----------------------------------------------------------------------
   				Restore Backup
 -----------------------------------------------------------------------
  1) Restore Backup Over The Network
  0) Return
 -----------------------------------------------------------------------
		";
		echo " ";

		read -p "Choose one option: " option2;
		case $option2 in

			0)
				clear;
				break;
				;;

			1)
				sudo chmod u+x scripts/restore/restore-ovn-script.sh
				./scripts/restore/restore-ovn-script.sh
				break
			;;
		esac
		done
		;;
esac
done
